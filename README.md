# SailGPS

![The GPS mounted on a boat](example.png "The GPS mounted on a boat")

A device for showing current speed and logging a GPS track, which is very useful to have when sailing dinghys.

## Hardware

Built for the stm32f1xx blue pill board using an [adafruit ultimate GPS]\*, a
[sharp memory display]\*\* and any generic SPI based SD card reader.

The `circuit_board` directory contains a schematic and a rough circuit board layout, though the latter
is very unfinished as I hand wired most of it.

\* Any GPS which outputs NMEA strings over UART should work

\*\* Currently, it is built for the deprecated 96x96 version, but will be
updated to the bigger non-deprecated version once that arrives in the mail

[adafruit ultimate GPS]: https://www.adafruit.com/product/746
[sharp memory display]: https://www.adafruit.com/product/1393



## Software

The included `.cargo/config` and `Embed.toml` file works for blue pill boards
with 128K of RAM meaning that `cargo embed --release` works out of the box. For
other f1 variants, those will have to be adjusted

The SD card should be formated with a FAT32 file system, and log files are
automatically written as soon as a GPS fix is found. The file names are
`ddHHMMSS` from when the first GPS fix was found to avoid file collisions while
staying under the 8 character filename limit.

The files contain the raw NMEA strings outputed by the GPS\*, those can be converted to GPS by my tool [nmea2gpx]

\* In order to avoid panicing during high loads, some bytes are thrown away
meaning that a few NMEA strings will be corrupted.

[nmea2gpx]: https://gitlab.com/TheZoq2/nmea2gpx
