use embedded_graphics::{
    draw_target::DrawTarget,
    pixelcolor::BinaryColor,
    prelude::{Dimensions, Point, Size},
};
use embedded_hal::blocking::delay::DelayMs;
use embedded_hal::blocking::spi::Write;
use embedded_hal::digital::v2::OutputPin;
use micromath::F32Ext;

use bit_reverse::LookupReverse;

// In LSB format
const WRITECMD: u8 = 0x80;
const VCOM: u8 = 0x40;
const CLEAR: u8 = 0x20;
const PADDING: u8 = 0;

#[derive(Debug)]
pub enum Error<SpiErr, PinErr> {
    PinError(PinErr),
    SpiError(SpiErr),
}

pub type Result<T, SpiErr, PinErr> = core::result::Result<T, Error<SpiErr, PinErr>>;

/// Driver for a sharp memory display. The SPI bus must run at <= 1 MHz clock
///
/// W and H represent the drawable area on the screen in pixels. LINE_BYTES is the amount
/// of bytes required to store a line of pixels with 1 bit color per pixel, i.e. W*SCALE//8
///
/// The SCALE parameter sets the amount of pixels on the display which correspond to one pixel.
/// i.e. SCALE=2 gives 2x2 pixels
pub struct SharpMemoryDisplay<
    Spi,
    CsPin,
    const W: usize,
    const H: usize,
    const SCALE: usize,
    const LINE_BYTES: usize,
> {
    spi: Spi,
    cs: CsPin,
    vcom: u8,

    lines: [[u8; LINE_BYTES]; H],
}

impl<Spi, CsPin, const W: usize, const H: usize, const SCALE: usize, const LINE_BYTES: usize>
    SharpMemoryDisplay<Spi, CsPin, W, H, SCALE, LINE_BYTES>
where
    Spi: Write<u8>,
    CsPin: OutputPin,
{
    pub fn new(
        spi: Spi,
        mut cs: CsPin,
        delay: &mut impl DelayMs<u16>,
    ) -> Result<Self, Spi::Error, CsPin::Error> {
        // Since we can't compute with const generics yet, we'll check a few things here

        // Micromath does not do full precision floating point math (or something), doing
        // 0.001 here should be fine unless you use *huge* scale values
        assert!(
            (SCALE as f32).log2().fract() < 0.001,
            "SCALE must be a power of two"
        );
        let expected_line_bytes = ((W * SCALE) as f32 / 8.).ceil() as usize;
        assert!(
            LINE_BYTES == expected_line_bytes,
            "LINE_BYTES should be W*SCALE / 8 rounded up ({}), got {}",
            expected_line_bytes,
            LINE_BYTES
        );

        // We could be more precise here, but the startup sequence should be fine
        // with 1 ms
        delay.delay_ms(1);
        cs.set_low().map_err(|e| Error::PinError(e))?;
        Ok(Self {
            spi,
            cs,
            vcom: VCOM,

            lines: [[0; LINE_BYTES]; H],
        })
    }

    pub fn draw_buffer(&mut self) -> Result<(), Spi::Error, CsPin::Error> {
        self.cs.set_high().map_err(|e| Error::PinError(e))?;
        self.spi
            .write(&[self.vcom | WRITECMD])
            .map_err(|e| Error::SpiError(e))?;

        for (i, line) in self.lines.iter().rev().enumerate() {
            for copy in 0..SCALE {
                self.spi
                    .write(&[(i as u8 * SCALE as u8 + copy as u8).swap_bits()])
                    .map_err(|e| Error::SpiError(e))?;
                self.spi.write(line).map_err(|e| Error::SpiError(e))?;
                self.spi.write(&[PADDING]).map_err(|e| Error::SpiError(e))?;
            }
        }
        self.spi.write(&[PADDING]).map_err(|e| Error::SpiError(e))?;

        self.vcom ^= VCOM;

        self.cs.set_low().map_err(|e| Error::PinError(e))?;

        Ok(())
    }

    pub fn clear(&mut self) -> Result<(), Spi::Error, CsPin::Error> {
        self.cs.set_high().map_err(|e| Error::PinError(e))?;

        self.spi
            .write(&[self.vcom | CLEAR])
            .map_err(|e| Error::SpiError(e))?;

        self.spi.write(&[PADDING]).map_err(|e| Error::SpiError(e))?;

        self.vcom ^= VCOM;

        self.cs.set_low().map_err(|e| Error::PinError(e))?;

        Ok(())
    }
}

impl<Spi, CsPin, const W: usize, const H: usize, const SCALE: usize, const LINE_BYTES: usize>
    Dimensions for SharpMemoryDisplay<Spi, CsPin, W, H, SCALE, LINE_BYTES>
{
    fn bounding_box(&self) -> embedded_graphics::primitives::Rectangle {
        embedded_graphics::primitives::Rectangle {
            top_left: Point::new(0, 0),
            size: Size::new(W as u32, H as u32),
        }
    }
}

impl<Spi, CsPin, const W: usize, const H: usize, const SCALE: usize, const LINE_BYTES: usize>
    DrawTarget for SharpMemoryDisplay<Spi, CsPin, W, H, SCALE, LINE_BYTES>
where
    Spi: Write<u8>,
    CsPin: OutputPin,
{
    type Color = BinaryColor;

    type Error = Error<Spi::Error, CsPin::Error>;

    fn draw_iter<I>(&mut self, pixels: I) -> core::result::Result<(), Self::Error>
    where
        I: IntoIterator<Item = embedded_graphics::Pixel<Self::Color>>,
    {
        for pixel in pixels {
            let line = pixel.0.y;

            for scale_bit in 0..(SCALE as i32) {
                let byte = ((W * SCALE) as i32 - pixel.0.x * SCALE as i32 + scale_bit) / 8;
                let bit = ((W * SCALE) as i32 - pixel.0.x * SCALE as i32 + scale_bit) % 8;

                let is_white = pixel.1 == BinaryColor::On;

                if line >= 0 && line < H as i32 && byte >= 0 as i32 && byte < (W * SCALE) as i32 / 8
                {
                    if !is_white {
                        self.lines[line as usize][byte as usize] |= 1 << (7 - bit);
                    } else {
                        self.lines[line as usize][byte as usize] &= !(1 << (7 - bit));
                    }
                }
            }
        }

        Ok(())

        // self.draw_buffer()
    }
}

#[macro_export]
macro_rules! draw_text {
    ($buffer:expr, $display:expr, $position:expr, $style:expr => $($arg:tt)*) => {
        $buffer.clear();
        ufmt::uwrite!($buffer, $($arg)*).unwrap();
        let t = Text::new(
            &$buffer,
            $position,
            $style
        );
        t.draw($display).unwrap()
    }
}
